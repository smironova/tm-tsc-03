package com.tsc.smironova.tm;

import com.tsc.smironova.tm.constant.TerminalConstant;

public class Application {

    public static void main(String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        parseArgs(args);
    }

    public static void parseArgs(String[] args) {
        if (args == null || args.length == 0)
            return;
        final String arg = args[0];
        if (TerminalConstant.ABOUT.equals(arg))
            showAbout();
        if (TerminalConstant.VERSION.equals(arg))
            showVersion();
        if (TerminalConstant.HELP.equals(arg))
            showHelp();
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Svetlana Mironova");
        System.out.println("E-MAIL: smironova@tsconsulting.com");
        System.exit(0);
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
        System.exit(0);
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalConstant.ABOUT + " - Display developer info.");
        System.out.println(TerminalConstant.VERSION + " - Display program version.");
        System.out.println(TerminalConstant.HELP + " - Display list of terminal commands.");
        System.exit(0);
    }

}
